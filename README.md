# OVinoRecherche

Allgemeines:
- Doku um Klassen besser als bei nivida :-) ( vor allem Beispiele ) - wahrscheinlich auch, weil die nicht dauernd alles umwerfen & deprectaten ( und HW ist ja auch
  weniger variantenreich)

- arbeitet intern dann eigtl. mit OpenCL-Kernels -> auch CPU-Support 

- OpenVINO™ integration with TensorFlow -> könnte man z.B. nutzen, um Problem hinzuschreiben und dann 1x mit OpenVino & 1x mit Cuda-Aware-Tensorflow zu vergleichen ?

- gibt eine Art "kumulatives virtuelles Gerät" - "nimm alles her, was Du findest, egal, ob GPU/CPU/beides"

Begriffe:
- IR = Intermediate Representation - noch nicht klar, ob das dann schon die "engine" ist oder ob der Loader da noch was intern macht

Toolchain:

- mo = ModelOptimizer -> frisst z.B. auch ONNX, beim export auch FP16 wie bei tensorRT-engines

- pot = Post-training-Optimization-Tool ( z.B. PythonNotebooks-113_.../114_... unten )

- benchmark_app -> kann man schnell mal Modelle im IR-Format zum Frass vorwerfen und sich dabei Inference-Geräte aussuchen ( z.B. PythonNotebooks-113_... unten )

- omz_downloader = OpenVino-Model-Zoo-Runterlader

Installation:

https://docs.openvino.ai/2021.4/openvino_docs_install_guides_installing_openvino_apt.html

https://medium.com/coding-blocks/how-to-install-openvino-on-ubuntu-18-04-e8d7d2dc1a8e


https://openbenchmarking.org/test/pts/openvino&eval=527fb123f0924e9935e9324a381cfc27a83dd625#metrics
https://openbenchmarking.org/test/pts/openvino
geht evtl. auch auf Ryzen notfalls

=========================================== alles:

https://github.com/orgs/openvinotoolkit/repositories


=========================================== Python-Notebooks

https://github.com/openvinotoolkit/openvino_notebooks

- gibt auch CPU als Zielgerät gleich ( aber muss AVX2,etc. haben - Ryzen möglich? )

- 001_...: Coco-Classification, Modelle als BIN & XML, ist dann das "OpenVino"-Engine format - hier gleich mal MobilenetV3 fertig -> gut als Tania-Basis

- 004_...: Detektion mit Boxes ( Texte erkennen )

- 101_...: tensorflow ins IR format bringen - zeigt mal die ModelOptimizer-Verwendung

- 102_...: exportiert ein Segmenation-Model (CoCo) in PyTorch erst mal in ein ONNX, konvertiert das dann in die IR und lässt beiden laufen -
           hat dabei auch schöne CPU/GPU-Umschaltungen ( lässt auch ONNX-Variante mit GPU laufen !)

- 104_...: ist das Modelzoo-Runterladen und durch-Benchmarken-Tool - hat am Ende auch eine CPU-und-GPU-Inference !

- 111_...: Beispiel FP32->INT8 quantization, zeigt auch "Post-Training-Optimization API" und macht Vergleich auch graphisch (fp32-box vs int8-box )

- 113_...: das hier ist die "accuracy-aware"-Quantisierung scheinbar, 114_... dann die Schnell-Schuss-Model-Optimiermethode
           wäre Vorlage für einen "Accuracy-Vergleicher", es steht im Readme, dass man da auch seine custom-Models reinbringen könnte
           hier auch wieder Torch->ONNX, das ONNX dann wieder mit den Model-Optimizer nach FP16 - es wird hier wieder Tania-Freundlich MobileNetV2 als Bespiel genommen
           aus FP16 dann durch Tool-Magie INT8

- 114_...: das hier ist dann die "simplified"-Quantisierung - schert sich nicht um Accuracy, nimmt Kalibrierbilder und macht -> gut zum Speed-Up-Potential sehen

- 115_...: async vs sync inference -> hier mal gutes Rahmenprogramm mit OpenCV-Fesnter und fps und Boxen-Malen

- 116_...: Beispiel für Sparsity - könnte hier aber sehr speziell auf einen bestimmte Xeon-Generation zugeschnitten sein - nimmt nnc
           (NeuralNetworkCompressionFramework) her, ist im Repo nebenan - Beispiel ist mit Sprachmodell (BERT)


- 212_...: Style-Transfer, wäre wieder ein ONNX-als-Basis-Beispiel

- 215_...: kann der mo Tensorflow-Models dann auch direkt lesen ?

- 220_...: hier wäre evtl. gezeigt, wie man YoloV5 ins IR-Format bekommt - mit wieder komplett anderen Werkzeugen aus extra Repo ("ultralitics")

- 226_...: hier wäre evtl. gezeigt, wie man YoloV7 ins IR-Format bekommt - mit ONNX-Zwischenweg

- 230_...: hier wäre evtl. gezeigt, wie man YoloV8 ins IR-Format bekommt - mit wieder komplett anderen Werkzeugen aus extra Repo ("ultralitics")

- 301_...: komplettes Beispiel mit Trainings-Zyklus in Tensorflow, weiteres Notebook demonstriert dann wieder Post-Training-Quantization

- 302_... & 305_...: gleich Quantization-Aware-Trainieren in PyTorch bzw. Tensorflow, braucht wieder nncf wie 116_...

- 401_...: Object detection mit Webcam, ssdlite_mobilenetv2

- 402_...: Pose estimation mit webcam, macht scheinbar auch mal mixed-precision fp16-int8 dabei

- 404_...: Style Transfer mit Webcam, interessant evtl. die ONNX-Basis

=========================================== eigtl. Toolkit ( mo z.B. und C/C++/Python-ohne-Notebooks )

https://github.com/openvinotoolkit/openvino.git

samples/c/hello_classification: klassifiziert Einzelbild mit bestimmen IR-Modell mit bestimmten Gerät, gibt Top-Ten-Liste aus - scheinbar nur mit AlexNet &
                                GoogleNet getestet

samples/cpp/hello_classification: wie in C, sehr viel weniger hinzuschreiben :-)

samples/cpp/classification_sample_async: nur googleNet evtl. - siehe Python-Notebooks/115_...

samples/cpp/benchmark_app: siehe PythonNotebooks-113_...

samples/cpp/hello_reshape_ssd: wieder gutes Beispiel für Boxen-Rausziehen

samples/cpp/model_creation_sample: zeigt, wie man mit der API selber ein Modell aufbaut ( wird dann aus Binärdatei mit Gewichten gefüttert )




